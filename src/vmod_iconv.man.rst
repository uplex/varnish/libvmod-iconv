..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit ./vmod_iconv.vcc and run make instead
..

.. role:: ref(emphasis)

==========
vmod_iconv
==========

------------------------------------
iconv character encoding conversions
------------------------------------

:Manual section: 3

SYNOPSIS
========

.. parsed-literal::

  import iconv [as name] [from "path"]
  
  new xconverter = iconv.converter(STRING from, STRING to)
  
      STRING xconverter.iconv(STRING, STRING fallback)
   

DESCRIPTION
===========

.. _UTF-8: https://en.wikipedia.org/wiki/UTF-8

.. _RFC9110: https://datatracker.ietf.org/doc/html/rfc9110#name-field-values

While at the time of release of this VMOD basically all of planet
earth has converged onto using `UTF-8`_ as the universal encoding for
digital text, other character sets still exist and, historically,
ISO-8859-1 encoding was even allowed in HTTP headers (see `RFC9110`_).

This VMOD provides access to the standard :ref:`iconv(3)` function for
character encoding conversion.

From/to conversion pairs are defined as objects, on which the
`xconverter.iconv()`_ method can be called.

Example
    ::

	import iconv;

	sub vcl_init {
		# "UTF-8" is default and can be left out
		new eurutf = iconv.converter(from="ISO-8859-15", to="UTF-8");
	}
	sub vcl_recv {
		set req.http.conv = eurutf.iconv(req.http.input);
	}

For cases where conversions might fail, a ``fallback`` argument can be
provided to the `xconverter.iconv()`_, which is returned instead of
triggering a VCL error for failures.

VMOD INTERFACE REFERENCE
========================

.. _iconv.converter():

new xconverter = iconv.converter(STRING from, STRING to)
--------------------------------------------------------

::

   new xconverter = iconv.converter(STRING from=0, STRING to=0)

Define an iconv converter from character set *from* to character set
*to*.

Left out arguments default to ``UTF-8``.

The permitted values are usually listed by the ``iconv --list``
command, see :ref:`iconv(1)`.

Typical examples for valid character sets are ``UTF-8`` and
``ISO-8859-15``.

Systems with the GNU C library and the GNU libiconv library support
the following two suffixes to character set names:

* ``//TRANSLIT``: Replace characters which can not be represented in
  the *to* character set with an approximation through one or several
  similarly looking characters.

* ``//IGNORE``: Silently discard characters which can not be
  represented in the *to* character set.

Use of invalid character sets fails loading the VCL.

.. _xconverter.iconv():

STRING xconverter.iconv(STRING, STRING fallback=0)
--------------------------------------------------

Run the converter on the given input. If conversion fails and
*fallback* is given, it is returned. Otherwise a VCL failure is
triggered.

SEE ALSO
========

* :ref:`vcl(7)`
* :ref:`varnishd(1)`
* :ref:`iconv(1)`
* :ref:`iconv_open(3)`
* :ref:`iconv(3)`

COPYRIGHT
=========

::

  Copyright 2023 UPLEX Nils Goroll Systemoptimierung
  All rights reserved
 
  Author: Nils Goroll <nils.goroll@uplex.de>
 
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.
