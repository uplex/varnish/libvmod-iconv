/*-
 * Copyright 2023 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <iconv.h>
#include <string.h>
#include <stdlib.h>

#include <cache/cache.h>

#include "vcc_iconv_if.h"

struct VPFX(iconv_converter) {
	unsigned		magic;
#define ICONV_CONVERTER_MAGIC	0x1ded5b1d
	char			*vcl_name;
	char			*from;
	char			*to;
};

static void
icfree(struct VPFX(iconv_converter) **icp)
{
	struct VPFX(iconv_converter) *ic;

	TAKE_OBJ_NOTNULL(ic, icp, ICONV_CONVERTER_MAGIC);
	//lint --e{774}
	REPLACE(ic->vcl_name, NULL);
	REPLACE(ic->from, NULL);
	REPLACE(ic->to, NULL);
	FREE_OBJ(ic);
}

VCL_VOID
vmod_converter__init(VRT_CTX, struct VPFX(iconv_converter) **icp,
    const char *vcl_name, VCL_STRING from, VCL_STRING to)
{
	struct VPFX(iconv_converter) *ic;
	iconv_t cd;

	AN(icp);
	AZ(*icp);

	if (from == NULL)
		from = "UTF-8";
	if (to == NULL)
		to = "UTF-8";

	cd = iconv_open(to, from);
	if (cd == (iconv_t)-1) {
		VRT_fail(ctx, "iconv.converter() failed: %s (%d)",
		    VAS_errtxt(errno), errno);
		return;
	}
	AZ(iconv_close(cd));

	ALLOC_OBJ(ic, ICONV_CONVERTER_MAGIC);
	if (ic == NULL) {
		VRT_fail(ctx, "iconv.converter() alloc failed");
		return;
	}

	REPLACE(ic->vcl_name, vcl_name);
	REPLACE(ic->from, from);
	REPLACE(ic->to, to);
	if (ic->vcl_name == NULL || ic->from == NULL || ic->to == NULL)
		icfree(&ic);
	else
		*icp = ic;
	return;
}

VCL_VOID
vmod_converter__fini(struct VPFX(iconv_converter) **icp)
{

	AN(icp);
	if (*icp == NULL)
		return;

	icfree(icp);
}

VCL_STRING
vmod_converter_iconv(VRT_CTX, struct VPFX(iconv_converter) *ic,
    VCL_STRANDS in, VCL_STRING fallback)
{
	const char *ret;
	char *outp, *inp;
	size_t convl, inl, outl;
	iconv_t cd;
	int i;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ic, ICONV_CONVERTER_MAGIC);
	(void)ic;
	AN(in);

	cd = iconv_open(ic->to, ic->from);
	if (cd == (iconv_t)-1) {
		VRT_fail(ctx, ".iconv() failed: %s (%d)",
		    VAS_errtxt(errno), errno);
		return (NULL);
	}

	inp = "<start>";
	convl = (size_t)-1;
	outl = WS_ReserveAll(ctx->ws);
	ret = outp = WS_Reservation(ctx->ws);
	for (i = 0; i < in->n; i++) {
		inp = TRUST_ME(in->p[i]);
		if (inp == NULL)
			continue;
		inl = strlen(inp);
		convl = iconv(cd, &inp, &inl, &outp, &outl);
		if (convl == (size_t)-1)
			break;
	}
	AZ(iconv_close(cd));

	// check space for NUL
	if (convl != (size_t)-1 && outl == 0)
		errno = E2BIG;

	if (convl == (size_t)-1 || outl == 0) {
		WS_Release(ctx->ws, 0);
		if (fallback) {
			VSLb(ctx->vsl, SLT_Error,
			    ".iconv() failed with %s (%d) at %s",
			    VAS_errtxt(errno), errno, inp);
		}
		else {
			VRT_fail(ctx, ".iconv() failed with %s (%d) at %s",
			    VAS_errtxt(errno), errno, inp);
		}
		return (fallback);
	}

	AN(outl);
	*outp = '\0';
	outp++;
	WS_ReleaseP(ctx->ws, outp);
	return (ret);
}
