====================================
iconv character encoding conversions
====================================

.. role:: ref(emphasis)

.. _Varnish-Cache: https://varnish-cache.org/

This project provides access to :ref:`iconv(3)` character encoding
conversions to VCL.

PROJECT RESOURCES
=================

* The primary repository is at https://code.uplex.de/uplex-varnish/libvmod-iconv

  This server does not accept user registrations, so please use ...

* the mirror at https://gitlab.com/uplex/varnish/libvmod-iconv for issues,
  merge requests and all other interactions.

DESCRIPTION
===========

.. _UTF-8: https://en.wikipedia.org/wiki/UTF-8

.. _RFC9110: https://datatracker.ietf.org/doc/html/rfc9110#name-field-values

While at the time of release of this VMOD basically all of planet
earth has converged onto using `UTF-8`_ as the universal encoding for
digital text, other character sets still exist and, historically,
ISO-8859-1 encoding was even allowed in HTTP headers (see `RFC9110`_).

This VMOD provides access to the standard :ref:`iconv(3)` function for
character encoding conversion.

From/to conversion pairs are defined as objects, on which the
`xconverter.iconv()`_ method can be called.

Example
    ::

	import iconv;

	sub vcl_init {
		# "UTF-8" is default and can be left out
		new eurutf = iconv.converter(from="ISO-8859-15", to="UTF-8");
	}
	sub vcl_recv {
		set req.http.conv = eurutf.iconv(req.http.input);
	}

For cases where conversions might fail, a ``fallback`` argument can be
provided to the `xconverter.iconv()`_, which is returned instead of
triggering a VCL error for failures.

Continue reading the documentation in `vmod_iconv(3) <src/vmod_iconv.man.rst>`_.

INSTALLATION
============

See `INSTALL.rst <INSTALL.rst>`_ in the source repository.

SUPPORT
=======

.. _gitlab.com issues: https://gitlab.com/uplex/varnish/libvmod-iconv/-/issues

To report bugs, use `gitlab.com issues`_.

For enquiries about professional service and support, please contact
info@uplex.de\ .

CONTRIBUTING
============

.. _merge requests on gitlab.com: https://gitlab.com/uplex/varnish/libvmod-iconv/-/merge_requests

To contribute to the project, please use `merge requests on gitlab.com`_.

To support the project's development and maintenance, there are
several options:

.. _paypal: https://www.paypal.com/donate/?hosted_button_id=BTA6YE2H5VSXA

.. _github sponsor: https://github.com/sponsors/nigoroll

* Donate money through `paypal`_. If you wish to receive a commercial
  invoice, please add your details (address, email, any requirements
  on the invoice text) to the message sent with your donation.

* Become a `github sponsor`_.

* Contact info@uplex.de to receive a commercial invoice for SWIFT payment.

COPYRIGHT
=========

::

  Copyright 2023 UPLEX Nils Goroll Systemoptimierung
  All rights reserved
 
  Author: Nils Goroll <nils.goroll@uplex.de>
 
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.
